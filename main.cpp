#include <iostream>
#include <iomanip>
using namespace std;

int main() {

    //"Överskrift vid start"
    cout << "===================================================" << endl;
    cout << "Bensinf\x94rbrukning och kostnad!!" << endl;
    cout << "Bensinpris = 15kr/L" << endl;
    cout << "===================================================" << endl;

    //deklarerar variabler i float form
    float prevgas;
    float nowgas;
    float liter;
    float antalkm;
    float antalmil;
    float litermil;
    float krmil;

    //skriver ut vad som skall skrivas och tar indatan för att skriva det till varibeln
    cout << "M\x84tarst\x84llning vid f\x94rra tankning [KM] :";
    cin >> prevgas;

    cout << "M\x84tarst\x84llning vid denna tankning [KM] :";
    cin >> nowgas;

    cout << "Fyll i antal liter vid tankning [L] :";
    cin >> liter;

    //matematisk funktion för att ta reda på antalkm för att sedan konvertera till mil.
    antalkm = nowgas - prevgas;
    antalmil = antalkm / 10;

    //tar reda på via en matematisk funktion liter/mil
    litermil = liter / antalmil;
    //sätter vad bensinpriset kostar och gångrar det med litermil
    krmil = litermil * 15;

    //setprecision(2) gör så att den endast skriver ut 2 decimaler. sedan skriver jag ut bensinförbrukningen och kostnad
    cout << setprecision (2) << fixed << "Bensinf\x94rbrukning [L/mil]: " <<  litermil << " Liter" << endl;
    cout << setprecision (2) << fixed << "Milkostnad [Kr/mil] : " << krmil << " Kr" << endl;


    return 0;
}
